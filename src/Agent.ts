import Distribution from "./Distribution";
import { getRandomInt } from "./util";

interface Agent {
    uid: string;
    x: number;
    y: number;
    wealth: number; // uniform [5, 25]
    sightRange: number; // uniform [1,6]
    metabolism: number; // uniform [1,4]
    move(distribution: Distribution): void;
}

class Agent implements Agent {

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
        this.wealth = getRandomInt(5, 25)
        this.metabolism = getRandomInt(1, 4)
        this.sightRange = getRandomInt(1, 6)
    }

    public move(distribution: Distribution): void {
        // from grid[y][x], find the cell with the most sugar
        // in adjacent K neighbors
        // this.y, this.x
        let s0 = distribution.cells[this.y][this.x] //TODO out of bounds error thrown

        //look along indices the agent's row
        for (let j = (-1 * this.sightRange); j < this.sightRange; j++) {
            const x1 = this.x + j
            if (x1 < 0 || x1 >= distribution.size) {
                continue
            }
            // look along adjacent rows with the same row index
            for (let i = (-1 * this.sightRange); i < this.sightRange; i++) {
                const y1 = this.y + i // y1 is the y coordinate of the cell we're looking at
                if (y1 < 0 || y1 >= distribution.size) { // check if y1 is out of bounds
                    continue
                }
                const s1 = distribution.cells[y1][x1] // s1 is the sugar content of the cell we're looking at
                if (s1 > s0) { // move to if the cell we're looking at has more sugar than the current max
                    s0 = s1
                    this.y = y1
                    this.x = x1
                }
            }
    }

        // [3] consumes M sugar from wealth
        this.wealth += distribution.cells[this.y][this.x]
        distribution.cells[this.y][this.x] = 0

        // [4] subtract metabolized sugar
        this.wealth -= this.metabolism
    }
}

export default Agent
