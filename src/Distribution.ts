const classic = (x: number, y: number) => {
    const { abs, sin, cos, floor } = Math
    return 1 + floor(abs( sin(x) * cos(y)) * 10)
}

// gives a cell's sugar as a function of its coordinates
export type SugarDistributionFunction = (x: number, y: number) => number;

interface Distribution {
    cells: number[][],
    size: number,
    replenishmentRate: number,
    replenish: () => void
}

interface DistributionConstructor {
    sugarDistributionFunction?: SugarDistributionFunction,
    size?: number
    replenishmentRate?: number
}

class Distribution implements Distribution {

    constructor({
        sugarDistributionFunction = classic,
        size = 51,
    }: DistributionConstructor = {}) {
        // create a grid of cells as a 2D array
        const cells: number[][] = []
        for (let y = 0; y < size; y++) {
            // create a row
            const row: number[] = [];
            for (let x = 0; x < size; x++) {
                // create a cell
                const sugar = sugarDistributionFunction(x, y)
                row.push(sugar)
            }
            cells.push(row)
        }

        this.cells = cells
        this.size = size
    }
}

export default Distribution
