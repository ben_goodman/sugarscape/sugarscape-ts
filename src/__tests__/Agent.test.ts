import { describe, test, expect } from '@jest/globals'

// import Grid from '../Grid'
import Agent from '../Agent'
import Distribution from '../Distribution';

const mockMath = Object.create(global.Math);
mockMath.random = () => 0.5;
global.Math = mockMath;

describe('Agent Constructor', () => {
    const agent = new Agent(0,0)
    test('Should have normal stats', () => {
        expect(agent.x).toBe(0)
        expect(agent.y).toBe(0)
        expect(agent.wealth).toBe(15)
        expect(agent.metabolism).toBe(3)
        expect(agent.sightRange).toBe(4)
    })
})

describe('Agent Class', () => {
    const sugarDistribution = new Distribution({
        sugarDistributionFunction: (x, y) => x == 1 && y == 1 ? 4 : 0,
        size: 5
    })
    const agent = new Agent(0,0)
    agent.sightRange = 5

    test('New location should maximize sugar', () => {
        agent.move(sugarDistribution)
        agent.move(sugarDistribution)
        // test grid at (0,2) has 4 sugar
        expect(agent.y).toBe(1)
        expect(agent.x).toBe(1)
        expect(agent.wealth).toBe(13) // 4 + (initial wealth - metabolism)
        // agent removes sugar from current location
        expect(sugarDistribution.cells[agent.y][agent.x]).toBe(0)
    })

})