import { describe, test, expect } from '@jest/globals'

import Distribution from '../Distribution'

describe('Distribution Class', () => {
    test('Should create a 2D array of cells', () => {
        const distribution = new Distribution()
        expect(distribution.cells.length).toBe(51)
        expect(distribution.cells[0].length).toBe(51)
    })
})
