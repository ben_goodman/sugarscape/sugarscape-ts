/**
 * https://runestone.academy/ns/books/published/complex/AgentBasedModels/Sugarscape.html
 */


import Canvas from './Canvas'
import Simulation from './Simulation'

const MAX_ITERATIONS = 500
const renderWidth = 700
const initialPopulation = 50
const gridSize = 51
const sugarReplenishmentFunction = () => 1
const sugarDistributionFunction = (x: number, y: number) => {
    // distance from x,y to center of grid
    return 1 / Math.sqrt((x - gridSize / 2) ** 2 + (y - gridSize / 2) ** 2) * 40
}

export const simulation = new Simulation({
    sugarDistributionFunction,
    sugarReplenishmentFunction,
    initialPopulation,
    gridSize
})

const canvas = new Canvas(renderWidth, simulation)


// main loop
canvas.drawFrame()

for (let iter = 0; iter < MAX_ITERATIONS; iter++) {
    (function() {
        setTimeout(() => {
            simulation.step()
            canvas.drawFrame()
        }, 400)
    })()
}


// export const sugarscape = {
//     step: simulation.step()
// }

// window["sugarscape"] = sugarscape



