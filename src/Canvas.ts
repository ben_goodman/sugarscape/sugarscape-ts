import Simulation from "./Simulation";

interface Canvas {
    renderWidth: number; // in pixels
    renderHeight: number; // in pixels
    simulation: Simulation;
    scale: number; // multiply by this to get cell's pixel dimensions
    canvas: HTMLCanvasElement;
    ctx: CanvasRenderingContext2D;
}

class Canvas implements Canvas {
    constructor(
        renderWidth: number,
        simulation: Simulation
    ) {
        const height = renderWidth;
        // this.distribution = simulation.distribution;
        this.simulation = simulation;

        const root = document.getElementById('root');
        this.canvas = document.createElement('canvas');
        this.canvas.width = renderWidth;
        this.canvas.height = height;
        root?.appendChild(this.canvas)

        this.canvas.width = renderWidth;
        this.canvas.height = height;
        this.ctx = this.canvas.getContext('2d')!;
        this.scale = 1; // size of cell will be renderWidth / distribution.width
    }

    private drawCells() {
        const cellWidth = Math.floor(this.canvas.width / this.simulation.distribution.size) * this.scale;
        this.ctx.strokeRect(0, 0, this.renderWidth, this.renderHeight);

        // draw cells as filled rectangles
        for (let i = 0; i < this.simulation.distribution.size; i++) {
            for (let j = 0; j < this.simulation.distribution.size; j++) {
                // create cell shape
                // const cell = new Path2D();
                // get cell color based on sugar content
                const sugar = this.simulation.distribution.cells[i][j]
                const saturation = sugar * 10 ;
                this.ctx.fillStyle = `hsl(20, ${saturation}%, 50%)`;
                this.ctx.fillRect(j * cellWidth, i * cellWidth, cellWidth, cellWidth);
            }
          }
    }

    private drawAgents() {
        const cellWidth = Math.floor(this.canvas.width / this.simulation.distribution.size) * this.scale;
        const agentShapeRadius = cellWidth / 2;

        // draw agents as circles centered at (x,y)
        this.simulation.agents.forEach(agent => {
            const agentShape = new Path2D();
            agentShape.arc(agent.x * cellWidth, agent.y * cellWidth, agentShapeRadius, 0, 2 * Math.PI);
            this.ctx.fillStyle = 'red';
            this.ctx.fill(agentShape);
        });
    }

    drawFrame() {
        this.drawCells();
        this.drawAgents();
    }
}

export default Canvas
