import Agent from "./Agent";
import Distribution, { type SugarDistributionFunction } from "./Distribution";
import { getRandomInt } from "./util";

interface Simulation {
    agents: Agent[];
    distribution: Distribution;
    sugarCellCapacity: number[][];
    sugarReplenishmentFunction: () => number;
    step(): void;
}

interface SimulationConstructor {
    sugarDistributionFunction?: SugarDistributionFunction;
    sugarReplenishmentFunction?: () => number;
    gridSize?: number;
    initialPopulation?: number;
}

class Simulation {

    constructor({
        sugarDistributionFunction,
        gridSize = 51,
        initialPopulation = 10,
        sugarReplenishmentFunction = () => 1
    }: SimulationConstructor = {}) {

        this.distribution = new Distribution({
            sugarDistributionFunction,
            size: gridSize,
        })

        this.sugarReplenishmentFunction = sugarReplenishmentFunction
        this.sugarCellCapacity = JSON.parse(JSON.stringify(this.distribution.cells))
        this.agents = []
        for (let i = 0; i < initialPopulation; i++) {
            const x = getRandomInt(0, gridSize - 1)
            const y = getRandomInt(0, gridSize - 1)
            this.agents.push(new Agent(x, y))
        }
    }

    step() {
        // move each agent
        this.agents.forEach(agent => agent.move(this.distribution))
        // cull agents with negative wealth
        this.agents = this.agents.filter(agent => agent.wealth > 0)

        // replenish sugar
        // After all agents have executed these steps, the cells grow back some sugar, typically 1 unit, but the total sugar in each cell is bounded by its capacity.
        this.distribution.cells = this.distribution.cells.map((row, j) => row.map((cell, i) =>
            {
                if ( cell < this.sugarCellCapacity[j][i]  ) {
                    return cell + this.sugarReplenishmentFunction()
                } else {
                    return cell
                }
            }
        ))
    }
}

export default Simulation
